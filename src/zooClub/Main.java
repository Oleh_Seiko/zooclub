package zooClub;

import java.util.*;

public class Main {
    public static void main(String[] args) {
        ZooClub zooClub = new ZooClub();
        fillZooClubSomeData(zooClub);
        printStartMenu();
        Scanner scanner = new Scanner(System.in);
        int number = scanner.nextInt();

        while (number != 0) {
            if (number == 1) {
                addMember(zooClub);
            }

            if (number == 2) {
                addAnimal(zooClub);
            }

            if (number == 3) {
                deleteAnimalInMember(zooClub);
            }
            if (number == 4) {
                deleteMember(zooClub);
            }

            if (number == 5) {
                deleteAnimalInAll(zooClub);
            }

            if (number == 6) {
                showZooClub(zooClub);
            }
            printNextMenu();
            number = scanner.nextInt();
        }
    }

    static void addMember(ZooClub zooClub) {
        System.out.println("Введіть імя учасника клубу");
        Scanner scanner1 = new Scanner(System.in);
        String firstName = scanner1.nextLine();
        Person person = new Person();
        person.setName(firstName);
        zooClub.getClub().put(person, null);
        System.out.println("Учасники клубу в MAP " + zooClub);
    }

    static void addAnimal(ZooClub zooClub) {
        System.out.println("Додайте тваринку до учасника клубу");
        System.out.println("Учасники клубу: ");
        Set<Person> listPerson = zooClub.getClub().keySet();
        for (Person person : listPerson) {
            System.out.println(person.getName());
        }
        System.out.println("Виберіть імя учасника до якого додати тварину");
        Scanner scan = new Scanner(System.in);
        String scanerPersonName = scan.nextLine();
        System.out.println("Введіть тварину, яку потрібно додати");
        Scanner sc = new Scanner(System.in);
        String scAnimalName = sc.nextLine();
        Person foundPerson = new Person();
        for (Person person : listPerson) {
            if (person.getName().equals(scanerPersonName)) {
                foundPerson = person;
            }
        }

        Map<Person, List<Pet>> club = zooClub.getClub();
        List<Pet> pets = club.get(foundPerson);
        List<Pet> pets2 = new ArrayList<>();
        pets2.addAll(pets);
        Pet pet = new Pet(scAnimalName);
        System.out.println("PET " + pet);
        System.out.println("PETS " + pets);
        pets2.add(pet);
        club.put(foundPerson, pets2);
    }

    static void deleteAnimalInMember(ZooClub zooClub) {
        System.out.println("Видалити тваринку з власника");
        System.out.println("Учасники клубу: ");
        Set<Person> personSet = zooClub.getClub().keySet();
        for (Person person : personSet) {
            System.out.println(person.getName());
        }
        System.out.println("Введіть імя учасника у якого видалити тваринку: ");
        Scanner scanner = new Scanner(System.in);
        String memberClub = scanner.nextLine();
        System.out.println("Введіть тварину, яку потрібно видалити ");
        Scanner scanner2 = new Scanner(System.in);
        String delPet = scanner2.nextLine();
        Person delPersonFound = new Person();
        for (Person person : personSet) {
            if (person.getName().equals(memberClub)) {
                delPersonFound = person;
            }
        }
        Map<Person, List<Pet>> clubDellPet = zooClub.getClub();
        List<Pet> petList = clubDellPet.get(delPersonFound);
        List<Pet> petList2 = new ArrayList<>();
        petList2.addAll(petList);
        Pet petsDel = new Pet(delPet);
        System.out.println("petsDel: " + petsDel);
        petList2.remove(petsDel);
        clubDellPet.put(delPersonFound, petList2);
        System.out.println("Delete Pet");

    }

    static void deleteMember(ZooClub zooClub) {
        System.out.println("Видалити учасника клубу");
        System.out.println("Учасники клубу: ");
        Set<Person> people = zooClub.getClub().keySet();
        for (Person person : people) {
            String namePerson = person.getName();
            System.out.println(namePerson);
        }
        System.out.println("Введіть імя учасника, якого потрібно видалити: ");
        Scanner scanner = new Scanner(System.in);
        String scEnterName = scanner.nextLine();
        Person personDelFound = new Person();
        for (Person person : people) {
            if (person.getName().equals(scEnterName)) {
                personDelFound = person;
            }
        }
        Map<Person, List<Pet>> club = zooClub.getClub();
        club.remove(personDelFound);
    }


    static void deleteAnimalInAll(ZooClub zooClub) {
        System.out.println("Видалити конкретну тваринку з усіх власників");
        System.out.println("Вибрати тварину, яку потрібно видалити:");
        Scanner scanner = new Scanner(System.in);
        String animalName = scanner.nextLine();
        Map<Person, List<Pet>> club = zooClub.getClub();
        Set<Map.Entry<Person, List<Pet>>> entrySet = club.entrySet();
        for (Map.Entry<Person, List<Pet>> entry : entrySet) {
            Person key = entry.getKey();
            List<Pet> petList = entry.getValue();
            List<Pet> modifiedPetList = new ArrayList<>();
            modifiedPetList.addAll(petList);
            Iterator<Pet> iterator = modifiedPetList.iterator();
            while (iterator.hasNext()){
                if (iterator.next().getName().equals(animalName)){
                    iterator.remove();
                }
            }
            club.put(key,modifiedPetList);
        }
    }


    static void showZooClub(ZooClub zooClub) {
        System.out.println("Вивести на екран зооклуб");
        Set<Map.Entry<Person, List<Pet>>> entries = zooClub.getClub().entrySet();
        for (Map.Entry<Person, List<Pet>> entry : entries) {
            String person = entry.getKey().getName();
            System.out.print(person + ": ");
            Iterator<Pet> iterator = entry.getValue().iterator();
            while (iterator.hasNext()) {
                String namePet = iterator.next().getName();
                if (iterator.hasNext()) {
                    System.out.print(namePet + ", ");
                } else {
                    System.out.print(namePet + ". ");
                }
            }
            System.out.println("");
        }
    }

    static void printStartMenu() {
        System.out.println("Меню");
        System.out.println("1) додати учасника в клуб");
        System.out.println("2) додати тваринку до учасника клубу");
        System.out.println("3) видалити тваринку з власника");
        System.out.println("4) видалити учасника клубу");
        System.out.println("5) видалити конкретну тваринку з усіх власників");
        System.out.println("6) вивести на екран зооклуб");
        System.out.println("0) вихід");
    }

    static void printNextMenu() {
        System.out.println("Оберіть наступну дію");
        System.out.println("1 - додати учасника в клуб");
        System.out.println("2 - додати тваринку до учасника клубу");
        System.out.println("3 - видалити тваринку з власника");
        System.out.println("4 - видалити учасника клубу");
        System.out.println("5 - видалити конкретну тваринку з усіх власників");
        System.out.println("6 - вивести на екран зооклуб");
        System.out.println("0 - вихід");
    }

    static void fillZooClubSomeData(ZooClub zooClub) {
        Person person1 = new Person("Yura");
        Person person2 = new Person("Kolya");
        Person person3 = new Person("Petya");
        Person person4 = new Person("Vasya");

        Pet pet1 = new Pet("Dog");
        Pet pet2 = new Pet("Parrot");
        Pet pet3 = new Pet("Cat");
        Pet pet4 = new Pet("Fish");
        Pet pet5 = new Pet("Crocodile");

        Map<Person, List<Pet>> club = zooClub.getClub();
        club.put(person1, Arrays.asList(pet1, pet4, pet2, pet3));
        club.put(person2, Arrays.asList(pet1, pet3));
        club.put(person3, Arrays.asList(pet1, pet3, pet5));
        club.put(person4, Arrays.asList(pet1, pet2, pet3, pet4, pet5));


    }
}
